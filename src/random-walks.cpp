#include <iostream>
#include <string>
#include <cmath>
#include <random>
#include <iomanip>

void showHelp(std::string binName);
std::vector<std::vector<double>> randomMatrix(int x, int y);
std::vector<std::vector<double>> cosMatrix(const std::vector<std::vector<double>>& x);
std::vector<std::vector<double>> sinMatrix(const std::vector<std::vector<double>>& x);
std::vector<std::vector<double>> cumSumMatrix(const std::vector<std::vector<double>>& x);
void printMatrix(std::vector<std::vector<double>> x);
double randomDouble();

int main(int argc, char* argv[]) {
	if (argc < 3) {
		showHelp(argv[0]);
		return 1;
	}
	int n = 0;
	int m = 0;
	for (int i = 1; i < argc; ++i) {
		std::string arg = argv[i];
		if ((arg == "-h") || (arg == "--help")) {
			showHelp(argv[0]);
			return 0;
		} else if (i == 1) {
			n = atoi(argv[i]);
		} else if (i == 2) {
			m = atoi(argv[i]);
		}
	}
	if (n <= 0 || m <= 0) {
		showHelp(argv[0]);
		return 1;
	}

	auto r = randomMatrix(n, m);
	auto c = cosMatrix(r);
	auto s = sinMatrix(r);
	auto x = cumSumMatrix(c);
	auto y = cumSumMatrix(s);
	printMatrix(r);
	printMatrix(c);
	printMatrix(s);
	printMatrix(x);
	printMatrix(y);
}

void showHelp(std::string binName) {
	std::cerr << "Runs a 2D random walk simulation"
	          << "Usage: " << binName << " [-h] N M\n"
			  << "\t N is the number of particles. A positive integer\n"
			  << "\t M is the number of iterations. A positive integer\n"
	          << "Options:\n"
			  << "\t -h, --help: Show this help message"
			  << std::endl;
}

std::vector<std::vector<double>> randomMatrix(int x, int y) {
	std::vector<double> zeros(y, 0);
	std::vector<std::vector<double>> matrix(x, zeros);
	for (std::vector<double>& col : matrix) {
		for (double& i : col) {
			i = randomDouble();
		}
	}
	return matrix;
}

double randomDouble() {
	static std::default_random_engine rng;
	static std::uniform_real_distribution<double> dist(0, 1);
	return dist(rng);
}

std::vector<std::vector<double>> cosMatrix(const std::vector<std::vector<double>>& x) {
	std::vector<double> zeros(x[0].size(), 0);
	std::vector<std::vector<double>> cosMx(x.size(), zeros);
	for (int i = 0; i < x.size(); i++) {
		for (int j = 0; j < x[i].size(); j++) {
			cosMx[i][j] = std::cos(2 * M_PI * x[i][j]);
		}
	}
	return cosMx;
}

std::vector<std::vector<double>> sinMatrix(const std::vector<std::vector<double>>& x) {
	std::vector<double> zeros(x[0].size(), 0);
	std::vector<std::vector<double>> sinMx(x.size(), zeros);
	for (int i = 0; i < x.size(); i++) {
		for (int j = 0; j < x[i].size(); j++) {
			sinMx[i][j] = std::sin(2 * M_PI * x[i][j]);
		}
	}
	return sinMx;
}

std::vector<std::vector<double>> cumSumMatrix(const std::vector<std::vector<double>>& x) {
	std::vector<double> zeros(x[0].size(), 0);
	std::vector<std::vector<double>> cumSum(x.size(), zeros);
	for (int i = 0; i < cumSum.size(); i++) {
		cumSum[i][0] = x[i][0];
	}
	for (int i = 0; i < cumSum.size(); i++) {
		for (int j = 1; j < cumSum[i].size(); j++) {
			cumSum[i][j] = cumSum[i][j-1] + x[i][j];
		}
	}
	return cumSum;
}

void printMatrix(std::vector<std::vector<double>> x) {
	std::cout << std::fixed << std::setprecision(2) << std::setw(5);
	for (auto row : x) {
		for (double i : row) {
			std::cout << i << " ";
		}
		std::cout << "\n";
	}
	std::cout << std::endl;
}
